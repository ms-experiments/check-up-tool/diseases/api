import Koa from 'koa';
import logger from 'koa-logger';
import bodyParser from 'koa-bodyparser';
import json from 'koa-json';
import cors from '@koa/cors';

import { router } from './router';
import { getConnection } from './mongo';

const port = process.env.PORT || 8000;
const app: Koa = new Koa();

app.use(json());
app.use(bodyParser());
app.use(cors());
app.use(logger());
app.use(router.routes()).use(router.allowedMethods());

async function bootstrap() {
  const dbClient = await getConnection();
  const db = await dbClient.db('diseases');

  app.context.db = db;

  app.listen(port, () => console.log('server running'));
}

bootstrap();
