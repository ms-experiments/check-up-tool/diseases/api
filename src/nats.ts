import { connect } from 'node-nats-streaming';

const clusterID: string = process.env.BROKER_CLUSTER_ID || 'test-cluster';
const clientID: string = process.env.BROKER_CLIENT_ID || 'test-client';
const url: string = process.env.BROKER_URL || 'nats://nats:4222';

export const stan = connect(clusterID, clientID, { url });
