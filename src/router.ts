import Router from 'koa-router';
import { Context } from 'koa';
import { v4 as uuid } from 'uuid';
import { stan } from './nats';

export const router = new Router({ prefix: '/diseases' });

router.get('/', async (ctx: Context) => {
  const { db }: Context = ctx;

  const result = await db.collection('diseases').find().toArray();

  ctx.body = { diseases: result };
});

router.post('/', async (ctx: Context) => {
  const { db }: Context = ctx;

  const name = ctx.request.body.name;

  if (!name) {
    ctx.body = { error: 'No name provided' };
    ctx.status = 403;
    return;
  }

  let disease = await db.collection('diseases').findOne({ name });

  if (disease) {
    ctx.body = { error: `Disease '${name}' already exists` };
    ctx.status = 403;
    return;
  }

  disease = {
    uuid: uuid(),
    name,
  };

  stan.publish('DISEASE_CREATED', JSON.stringify(disease));

  ctx.body = { disease };
});
