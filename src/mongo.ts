import { MongoClient } from 'mongodb';

const url: string = process.env.CACHE_URL || 'mongodb://localhost:27017';

export async function getConnection(): Promise<MongoClient> {
  try {
    const client: MongoClient = new MongoClient(url);
    await client.connect();
    await client.db('admin').command({ ping: 1 });
    console.log('Connect successfully to database');
    return client;
  } catch (e) {
    console.log('Failed connect to database');
    console.log(e);
    process.exit(1);
  }
}
